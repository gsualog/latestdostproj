<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>Maintenance Checklist</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <style type="text/css">
        .successModal{
        font-size: 40px;
        text-align: center;
          }

      .add{
        font-size:15px;
      }

      .add1{
        padding-left: 12px;
      }
      body{
        font-family: 'Roboto', sans-serif;
        font-weight:100;
      }

      .page-header{
        padding-top: 30px;

      }

      .dropwdown{
        width: 100%;

      }

      .dropdown-toggle{
        width: 100%;

      }
      .dropdown-menu{
        width: 100%;

      }
      /*.panel-heading p{
        color: black
      }*/
      @import "compass/css3";

    h2 {
      font: 400 40px/1.5 Helvetica, Verdana, sans-serif;
      margin: 0;
      padding: 0;
    }

    .panel-heading ul {
      list-style-type: none;
      margin: 0;
      padding: 0;
    }

    .panel-heading li {
      font: 200 20px/1.5 Helvetica, Verdana, sans-serif;
      border-bottom: 1px solid #ccc;
    }

    .panel-heading li:last-child {
      border: none;
    }

    .panel-heading li {
      text-decoration: none;
      color: #000;

      -webkit-transition: font-size 0.3s ease, background-color 0.3s ease;
      -moz-transition: font-size 0.3s ease, background-color 0.3s ease;
      -o-transition: font-size 0.3s ease, background-color 0.3s ease;
      -ms-transition: font-size 0.3s ease, background-color 0.3s ease;
      transition: font-size 0.3s ease, background-color 0.3s ease;
      display: block;
      /*width: px;*/
    }
     #category{
      font-size: 16px;
    }
    strong{
      text-decoration: underline;
      font-size: 18px;
    }

   /* .panel-heading li:hover {
      font-size: 30px;
      background: #f6f6f6;
    }*/
     
    </style>
</head>
<body>
  <nav class="navbar navbar-inverse navbar-static-top">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

                    <!-- Branding Image -->
                <a class="navbar-brand" href="/home">
                  Maintenance Checklist
                </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                &nbsp;
            </ul>
             <!-- Right Side Of Navbar -->
             <ul class="nav navbar-nav navbar-right">
                 <!-- Authentication Links -->
                
                     <li class="dropdown">
                         <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            Admin<span class="caret"></span>
                         </a>
                          <ul class="dropdown-menu" role="menu">
                                <li><a href="/admin/home">Home</a></li>
                          </ul>
                     </li>
                 
             </ul>
         </div>
    </nav>

<div class="container">
  <div class="row">
      <div class="col-md-10 col-md-offset-1">
          <h2 class="col-md-offset-4">Archived Items</h2>
      
        <div class="panel panel-default">
          
          <div class="panel-heading">

            @foreach($users as $user)
            <span id = "user"><strong>{{ $user->name }}</strong><span>
              <table class="table table-striped">
                <thead>
                  <th class="col-md-4">Item Code</th>
                  <th class="col-md-4">Reason</th>
                  <th class="col-md-4">Category</th>
                </thead>
                <tbody>
                @for ($i=0; $i <=count($users); $i++)
                  @forelse($items as $item)
                    @if($item->office_id == $i && $user->id==$i )
                      <tr>
                         <td>{{ $item->code }}</td>
                         <td>{{ $item->reason }}</td>
                         <td>{{ $item->name }}</td>
                      </tr>
                    @endif
                  @empty
                    No results found
                  @endforelse
                @endfor
              </tbody>
            </table>
            @endforeach
          </div>
        </div>
      </div>
  </div>  
</div>
</body>
</html>