<!doctype html>
<html>
<head>
    <title>Maintenance Checklist</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <style type="text/css">
        .successModal{
        font-size: 40px;
        text-align: center;
          }

      .add{
        font-size:15px;
      }

      .add1{
        padding-left: 12px;
      }
      body{
        font-family: 'Roboto', sans-serif;
        font-weight:100;
      }

      .page-header{
        padding-top: 30px;

      }

      .dropwdown{
        width: 100%;

      }

      .dropdown-toggle{
        width: 100%;

      }
      .dropdown-menu{
        width: 100%;

      }
     .modal.fade{
      width:auto;
      height:auto;
     }
    </style>
</head>
<body>
@include('layouts.userhomeApp')
<div class="page-header text-center">
  <h1 class="">Maintenance Forms</h1>

</div>

<div class="container">
<div class="jumbotron center maintenance">
    <div class="container">
         <!-- <form class="form-horizontal" role="form" action="#" method="POST"> -->
           <div class="row center-block"> 
            <div class="column">
                  <div class="row">
                    <div class=" pull-right">
                     <label id="date">Date: YY-MM-DD</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="pull-left">
                      <a href="{{url('/items/create')}}" class="btn btn-md btn-info"><span class="glyphicon glyphicon-plus-sign"></span> Add </a>
                    </div>

                    <div class=" pull-right">
                     <a href="{{url('/items/archive')}}" class="btn btn-md btn-info"><span class="glyphicon glyphicon-briefcase"></span> Archive </a>
                     
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-3">
                      <h3><b>Item code</b></h3>
                    </div>
                    <div class="col-md-3">
                      <h3><b>&nbsp;In Good Condition?</b></h3>
                    </div>
                    <div class="col-md-3">
                      <h3><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Details</b></h3>
                    </div>
                  </div>
                  <h4><b>Structure</b></h4>
                  @forelse($items as $item)
                      @if($item->category_id === 1)
                  <div class="row">

                      <input type="hidden" name="item_id" value="{{ $item->code}}">
                      <input type="hidden" name="office_id" value="{{ Auth::user()->id }}">
                      <div class="col-md-3">
                       <input type="text" class="form-control" name="itemcode" value="{{ $item->code}}" readonly="readonlya">
                      </div>
                    
                      <div class="col-md-3">
                        <div class="form-group text-center">
                          <input type="radio" name="condition{{$item->code}}" value="1"><small>Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small>
                          <input type="radio" name="condition{{$item->code}}" value="0"><small>No</small>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                          <div class="col-md-12">
                            <input type="text" class="form-control" id="usr" name="details">
                          </div>
                        </div>
                     </div>

                    <div class="col-md-3">
                      
                        <button class="btn btn-danger"  onclick="message({{$item->id}})"><span class="glyphicon glyphicon-trash"></span></button>
                      
                    </div>
                  </div>
                   @endif
                  @empty
                    <h3 class="text-center text-muted">No results found</h3>
                  @endforelse
                  <hr style="width: 100%; background-color: black; height: 1px; border-color : transparent;" />

                  <h4><b>Furniture</b></h4>
                  @forelse($items as $item)
                      @if($item->category_id === 2)
                  <div class="row">

                      <input type="hidden" name="item_id" value="{{ $item->code}}">
                      <input type="hidden" name="office_id" value="{{ Auth::user()->id }}">
                      <div class="col-md-3">
                       <input type="text" class="form-control" name="itemcode" value="{{ $item->code}}" readonly="readonly">
                      </div>
                    
                      <div class="col-md-3">
                        <div class="form-group text-center">
                          <input type="radio" name="condition{{$item->code}}" value="1"><small>Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small>
                          <input type="radio" name="condition{{$item->code}}" value="0"><small>No</small>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                          <div class="col-md-12">
                            <input type="text" class="form-control" id="usr" name="details">
                          </div>
                        </div>
                     </div>

                    <div class="col-md-3">
                       <button class="btn btn-danger"  onclick="message({{$item->id}})"><span class="glyphicon glyphicon-trash"></span></button>
                    </div>
                  </div>
                   @endif
                  @empty
                    <h3 class="text-center text-muted">No results found</h3>

                  @endforelse
                  <hr style="width: 100%; background-color: black; height: 1px; border-color : transparent;" />

                  <h4><b>IT Equipments</b></h4>
                  @forelse($items as $item)
                      @if($item->category_id === 3)
                  <div class="row">

                      <input type="hidden" name="item_id" value="{{ $item->code}}">
                      <input type="hidden" name="office_id" value="{{ Auth::user()->id }}">
                      <div class="col-md-3">
                       <input type="text" class="form-control" id="usr" name="details" value="{{ $item->code}}" readonly="readonly">
                      </div>
                    
                      <div class="col-md-3">
                        <div class="form-group text-center">
                          <input type="radio" name="condition{{$item->code}}" value="1"><small>Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small>
                          <input type="radio" name="condition{{$item->code}}" value="0"><small>No</small>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                          <div class="col-md-12">
                            <input type="text" class="form-control" id="usr" name="details">
                          </div>
                        </div>
                     </div>

                    <div class="col-md-3">

                       <button class="btn btn-danger"  onclick="message({{$item->id}})"><span class="glyphicon glyphicon-trash"></span></button>

                    </div>
                  </div>
                   @endif
                  @empty
                    <h3 class="text-center text-muted">No results found</h3>

                  @endforelse
                  <hr style="width: 100%; background-color: black; height: 1px; border-color : transparent;" /> 

                  <h4><b>Emergency Equipments</b></h4>
                  @forelse($items as $item)
                      @if($item->category_id === 4)
                  <div class="row">

                      <input type="hidden" name="item_id" value="{{ $item->code}}">
                      <input type="hidden" name="office_id" value="{{ Auth::user()->id }}">
                      <div class="col-md-3">
                       <input type="text" class="form-control" id="usr" name="details" value="{{ $item->code}}" readonly="readonly">
                      </div>
                    
                      <div class="col-md-3">
                        <div class="form-group text-center">
                          <input type="radio" name="condition{{$item->code}}" value="1"><small>Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small>
                          <input type="radio" name="condition{{$item->code}}" value="0"><small>No</small>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                          <div class="col-md-12">
                            <input type="text" class="form-control" id="usr" name="details">
                          </div>
                        </div>
                     </div>

                    <div class="col-md-3">

                       <button class="btn btn-danger"  onclick="message({{$item->id}})"><span class="glyphicon glyphicon-trash"></span></button>
                       
                    </div>
                  </div>
                   @endif
                  @empty
                    <h3 class="text-center text-muted">No results found</h3>

                  @endforelse
                  <hr style="width: 100%; background-color: black; height: 1px; border-color : transparent;" />

                  <h4><b>Comfort Room</b></h4>
                  @forelse($items as $item)
                      @if($item->category_id === 5)
                  <div class="row">

                      <input type="hidden" name="item_id" value="{{ $item->code}}">
                      <input type="hidden" name="office_id" value="{{ Auth::user()->id }}">
                      <div class="col-md-3">
                       <input type="text" class="form-control" id="usr" name="details" value="{{ $item->code}}" readonly="readonly">
                      </div>
                    
                      <div class="col-md-3">
                        <div class="form-group text-center">
                          <input type="radio" name="condition{{$item->code}}" value="1"><small>Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small>
                          <input type="radio" name="condition{{$item->code}}" value="0"><small>No</small>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                          <div class="col-md-12">
                            <input type="text" class="form-control" id="usr" name="details">
                          </div>
                        </div>
                     </div>

                    <div class="col-md-3">
                       <button class="btn btn-danger"  onclick="message({{$item->id}})"><span class="glyphicon glyphicon-trash"></span></button>
                    </div>
                  </div>
                   @endif
                  @empty
                    <h3 class="text-center text-muted">No results found</h3>

                  @endforelse
                  <hr style="width: 100%; background-color: black; height: 1px; border-color : transparent;" />
                  <h4><b>Pantry</b></h4>
                  @forelse($items as $item)
                      @if($item->category_id === 6)
                  <div class="row">

                      <input type="hidden" name="item_id" value="{{ $item->code}}">
                      <input type="hidden" name="office_id" value="{{ Auth::user()->id }}">
                      <div class="col-md-3">
                       <input type="text" class="form-control" id="usr" name="details" value="{{ $item->code}}" readonly="readonly">
                      </div>
                    
                      <div class="col-md-3">
                        <div class="form-group text-center">
                          <input type="radio" name="condition{{$item->code}}" value="1"><small>Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small>
                          <input type="radio" name="condition{{$item->code}}" value="0"><small>No</small>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                          <div class="col-md-12">
                            <input type="text" class="form-control" id="usr" name="details">
                          </div>
                        </div>
                     </div>

                    <div class="col-md-3">
                       <button class="btn btn-danger"  onclick="message({{$item->id}})"><span class="glyphicon glyphicon-trash"></span></button>
                    </div>
                  </div>
                   @endif
                  @empty
                    <h3 class="text-center text-muted">No results found</h3>

                  @endforelse
            </div>
            <div class="row">
              <div class=" pull-right">
               <button type="submit" class="btn btn-success " data-toggle="modal" data-target="#addModal"> <span  class="glyphicon glyphicon-send"> Submit </span></button>
                 <!--<button type="button" class="btn btn-danger glyphicon glyphicon-remove-circle">Clear</button>-->
              </div>
            </div>
          <!-- </form> -->
        </div>
    </div>
</div>
</div>

<div id="addModal" class="modal fade" role="dialog">
  <div class="modal-dialog ">

    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title">Are you sure you want to send this?</h3>
      </div>
      <div class="modal-body">
        
    <div class="container">
         <!-- <form class="form-horizontal" role="form" action="#" method="POST"> -->
           <div class="row center-block"> 
            <div class="column">
                  <div class="row">
                    <div class=" pull-right">
                     <label>Date: mm/dd/yy</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-3 col-sm-3">
                      <h4><b>Item code</b></h4>
                    </div>
                    <div class="col-md-3 col-sm-3">
                      <h4><b>&nbsp;In Good Condition?</b></h4>
                    </div>
                    <div class="col-md-3 col-sm-3">
                      <h3><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Details</b></h3>
                    </div>
                  </div>
                  <h4><b>Structure</b></h4>
                  @forelse($items as $item)
                      @if($item->category_id === 1)
                  <div class="row">

                      <input type="hidden" name="item_id" value="{{ $item->code}}">
                      <input type="hidden" name="office_id" value="{{ Auth::user()->id }}">
                      <div class="col-md-3 col-sm-4">
                       <input type="text" class="form-control" name="itemcode" value="{{ $item->code}}" readonly="readonly">
                      </div>
                    
                      <div class="col-md-3 col-sm-4">
                        <div class="form-group text-center">
                          <input type="radio" name="condition{{$item->code}}" value="1"><small>Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small>
                          <input type="radio" name="condition{{$item->code}}" value="0"><small>No</small>
                        </div>
                      </div>

                      <div class="col-md-3 col-sm-4">
                        <div class="form-group">
                          <div class="col-md-12">
                            <input type="text" class="form-control" id="usr" name="details" readonly="readonly">
                          </div>
                        </div>
                     </div>
                  </div>
                   @endif
                  @empty
                    <h3 class="text-center text-muted">No results found</h3>
                  @endforelse
                  <hr style="width: 100%; background-color: black; height: 1px; border-color : transparent;" />

                  <h4><b>Furniture</b></h4>
                  @forelse($items as $item)
                      @if($item->category_id === 2)
                  <div class="row">

                      <input type="hidden" name="item_id" value="{{ $item->code}}">
                      <input type="hidden" name="office_id" value="{{ Auth::user()->id }}">
                      <div class="col-md-3">
                       <input type="text" class="form-control" name="itemcode" value="{{ $item->code}}" readonly="readonly">
                      </div>
                    
                      <div class="col-md-3">
                        <div class="form-group text-center">
                          <input type="radio" name="condition{{$item->code}}" value="1"><small>Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small>
                          <input type="radio" name="condition{{$item->code}}" value="0"><small>No</small>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                          <div class="col-md-12">
                            <input type="text" class="form-control" id="usr" name="details">
                          </div>
                        </div>
                     </div>
                  </div>
                   @endif
                  @empty
                    <h3 class="text-center text-muted">No results found</h3>

                  @endforelse
                  <hr style="width: 100%; background-color: black; height: 1px; border-color : transparent;" />

                  <h4><b>IT Equipments</b></h4>
                  @forelse($items as $item)
                      @if($item->category_id === 3)
                  <div class="row">

                      <input type="hidden" name="item_id" value="{{ $item->code}}">
                      <input type="hidden" name="office_id" value="{{ Auth::user()->id }}">
                      <div class="col-md-3">
                       <input type="text" class="form-control" id="usr" name="details" value="{{ $item->code}}" readonly="readonly">
                      </div>
                    
                      <div class="col-md-3">
                        <div class="form-group text-center">
                          <input type="radio" name="condition{{$item->code}}" value="1"><small>Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small>
                          <input type="radio" name="condition{{$item->code}}" value="0"><small>No</small>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                          <div class="col-md-12">
                            <input type="text" class="form-control" id="usr" name="details">
                          </div>
                        </div>
                     </div>
                  </div>
                   @endif
                  @empty
                    <h3 class="text-center text-muted">No results found</h3>

                  @endforelse
                  <hr style="width: 100%; background-color: black; height: 1px; border-color : transparent;" /> 

                  <h4><b>Emergency Equipments</b></h4>
                  @forelse($items as $item)
                      @if($item->category_id === 4)
                  <div class="row">

                      <input type="hidden" name="item_id" value="{{ $item->code}}">
                      <input type="hidden" name="office_id" value="{{ Auth::user()->id }}">
                      <div class="col-md-3">
                       <input type="text" class="form-control" id="usr" name="details" value="{{ $item->code}}" readonly="readonly">
                      </div>
                    
                      <div class="col-md-3">
                        <div class="form-group text-center">
                          <input type="radio" name="condition{{$item->code}}" value="1"><small>Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small>
                          <input type="radio" name="condition{{$item->code}}" value="0"><small>No</small>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                          <div class="col-md-12">
                            <input type="text" class="form-control" id="usr" name="details">
                          </div>
                        </div>
                     </div>
                  </div>
                   @endif
                  @empty
                    <h3 class="text-center text-muted">No results found</h3>

                  @endforelse
                  <hr style="width: 100%; background-color: black; height: 1px; border-color : transparent;" />

                  <h4><b>Comfort Room</b></h4>
                  @forelse($items as $item)
                      @if($item->category_id === 5)
                  <div class="row">

                      <input type="hidden" name="item_id" value="{{ $item->code}}">
                      <input type="hidden" name="office_id" value="{{ Auth::user()->id }}">
                      <div class="col-md-3">
                       <input type="text" class="form-control" id="usr" name="details" value="{{ $item->code}}" readonly="readonly">
                      </div>
                    
                      <div class="col-md-3">
                        <div class="form-group text-center">
                          <input type="radio" name="condition{{$item->code}}" value="1"><small>Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small>
                          <input type="radio" name="condition{{$item->code}}" value="0"><small>No</small>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                          <div class="col-md-12">
                            <input type="text" class="form-control" id="usr" name="details">
                          </div>
                        </div>
                     </div>
                  </div>
                   @endif
                  @empty
                    <h3 class="text-center text-muted">No results found</h3>

                  @endforelse
                  <hr style="width: 100%; background-color: black; height: 1px; border-color : transparent;" />
                  <h4><b>Pantry</b></h4>
                  @forelse($items as $item)
                      @if($item->category_id === 6)
                  <div class="row">

                      <input type="hidden" name="item_id" value="{{ $item->code}}">
                      <input type="hidden" name="office_id" value="{{ Auth::user()->id }}">
                      <div class="col-md-3">
                       <input type="text" class="form-control" id="usr" name="details" value="{{ $item->code}}" readonly="readonly">
                      </div>
                    
                      <div class="col-md-3">
                        <div class="form-group text-center">
                          <input type="radio" name="condition{{$item->code}}" value="1"><small>Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small>
                          <input type="radio" name="condition{{$item->code}}" value="0"><small>No</small>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                          <div class="col-md-12">
                            <input type="text" class="form-control" id="usr" name="details">
                          </div>
                        </div>
                     </div>
                  </div>
                   @endif
                  @empty
                    <h3 class="text-center text-muted">No results found</h3>

                  @endforelse
            </div>
            <div class="row">
              <div class=" pull-right">
               <button type="submit" class="btn btn-success " data-toggle="modal" data-target="#addModal"> <span  class="glyphicon glyphicon-send"> Submit </span></button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button> <!--<button type="button" class="btn btn-danger glyphicon glyphicon-remove-circle">Clear</button>-->
              </div>
            </div>
          <!-- </form> -->
        </div>
    </div>
</div>
        </div>
        </div>
        
      <!-- <div class="modal-footer">
        
      </div> -->
    </div>
    </div>

<script type="text/javascript">
        
            function getConfirmation(){
               var retVal = confirm("Do you want to continue ?");
               if( retVal == true ){
                  document.write ("User wants to continue!");
                  return true;
               }
               else{
                  document.write ("User does not want to continue!");
                  return false;
               }
            }

            function message(item_id)
            {
              // item_id = $(this).attr("id");
              if (confirm('Are you sure you want to Delete this Item?')) {
                window.location = "/items/delete/"+ item_id;
              } else {
                window.location = "/items";
              }
            }
         
</script>
</body>

