<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <title>Add Items</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
        <style type="text/css">
            @import url(https://fonts.googleapis.com/css?family=Roboto:400,300,100,700,500);
            .search{
                text-align: center;
            }

            .admin{
                padding-left: 50px;
            }
            .toolbarRight{
                padding-right: 20px;
            }
            .addItems{
                padding-top: 70px;
            }
            body{
                font-family: 'Roboto', sans-serif;
                font-weight:325;
            }
        </style>
    </head>
    <body>
       @include('layouts.app')
    <div class="container">
    <p class="addItems" style="font-size:30px;">Add Items</p>
    <hr>
    <div class="row">
      <div class="col-md-12 personal-info jumbotron">
        
        <form class="form-horizontal" role="form" action="/items" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="office_id" value="{{ Auth::user()->id }}" />
            <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                <p class="col-lg-3 control-label">Item Code</p>    

                <div class="col-md-8">
                <input id="code" type="text" class="form-control" name="code" value="{{ old('code') }}" style="width:500px;" required autofocus>

                @if ($errors->has('code'))
                    <span class="help-block">
                      <strong>{{ $errors->first('code') }}</strong>
                    </span>
                @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                <p class="col-lg-3 control-label">Category</p> 
                    <div class="col-md-8">
                        <select id="category_id" class="form-control" name="category_id" style="width:500px;" required autofocus>
                            <option value="">--- Select Category ---</option>
                            @foreach ($cats as $cat)
                                <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('category_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('category_id') }}</strong>
                            </span>
                        @endif
                    </div>
            </div>
         
                <input id="deleted" type="hidden" name="deleted" value="0" />
                <input id="reason" type="hidden" name="reason" value="null" />
                <button type="submit" class="btn btn-success pull-right">
                  <i class="icon-user icon-white glyphicon glyphicon-plus"></i>&nbsp;Add
                </button>
          </form>
      </div>
  </div>
</div>
</body>
</html>

