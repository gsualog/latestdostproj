<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <title>Add Items</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
        <style type="text/css">
            @import url(https://fonts.googleapis.com/css?family=Roboto:400,300,100,700,500);
            .search{
                text-align: center;
            }

            .admin{
                padding-left: 50px;
            }
            .toolbarRight{
                padding-right: 20px;
            }
            .addItems{
                padding-top: 70px;
            }
            body{
                font-family: 'Roboto', sans-serif;
                font-weight:325;
            }
        </style>
    </head>
    <body>
      @include('layouts.app')
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3>Why remove this item {{$item->code}} ?</h3>
				</div>
				<div class="panel-body panel-default">
					<form class="form-delete" action="/items/{{ $item->id }}" method="post">
						{{ csrf_field() }}
						{{ method_field('DELETE') }}
						<textarea name="reason" class="form-control" placeholder="State your reason"></textarea>
						</br>
						<input type="submit" class="btn btn-danger pull-right" value="Delete" />
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>