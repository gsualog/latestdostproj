<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>Maintenance Checklist</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <style type="text/css">
       
      body{
        font-family: 'Roboto', sans-serif;
        font-weight:100;
      }

      .panel-heading p{
        color: black
      }

      .glyphicon-lg{font-size:5em}
      .blockquote-box{border-right:5px solid #E6E6E6;margin-bottom:30px;margin-top: 40px;}
      .blockquote-box.blockquote-primary{border-color:#357EBD}
      .blockquote-box.blockquote-primary .square{background-color:#428BCA;color:#FFF}
     
      .btn-label {position: relative;
                left: -10px;
                display: inline-block;
                padding: 6px 12px;
                background: rgba(0,0,0,0.15);
                border-radius: 3px 0 0 3px;}
      .btn-labeled {padding-top: 0;padding-bottom: 0;}
    </style>
</head>
<body>
  @include('layouts.adminApp')
    <div class="container">
      <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class=" blockquote-box blockquote-primary clearfix">
                <div class="square pull-left">
                    <span class="glyphicon glyphicon-user glyphicon-lg"></span>
                </div>
                <h1 style="text-align:center;">List Of Offices</h1>
        </div>
        
        @forelse($users as $user)
        <div class="panel panel-default">

          <div class="panel-heading">
            <h4><b>Office {{ $user->id }}</b></h4>
          </div>
          <div class="panel-body">
              <!-- {{ $user->id }}. &nbsp; -->
            <p>{{ $user->name }}</p>
              <button type="button" class="btn  btn-labeled btn-success pull-right"  onclick="toEdit( {{ $user->id }} )">
                          <span class="btn-label"><i class="glyphicon glyphicon-pencil"></i></span>Edit</button>
              <!-- <button class="btn btn-success pull pull-right" onclick="toEdit( {{ $user->id }} )"><span class="glyphicon glyphicon-pencil"></span>&nbsp;Edit</button> -->
              <!-- <input type="submit" class="btn btn-success pull pull-right" value="Edit" onclick="toEdit( {{ $user->id }} )"/> -->

              <script type="text/javascript">
        
              function toEdit(user_id)
              {
                  window.location = "/user/settings/"+ user_id;
                // glyphicon glyphicon-pencil
              }
              </script>
          </div>
        </div>
        @empty
          No results found
        @endforelse
      </div>
    </div>
    </div>
</body>
</html>