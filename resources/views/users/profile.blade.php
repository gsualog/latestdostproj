<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <title>ADMIN</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
        <style type="text/css">
            @import url(https://fonts.googleapis.com/css?family=Roboto:400,300,100,700,500);
            body{
                font-family: 'Roboto', sans-serif;
                font-weight:325;
            }
            .panel-footer{
				height:60px;
			}
        </style>
    </head>

<body>
@include(layouts.adminApp)
<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="panel panel-default">
				<div class="panel-body ">
					<h4>Name of Office: {{ Auth::user()->name }} </h4>
					<h4>Password: <input type="hidden" value="{{ Auth::user()->password }}"/></h4>
				</div>
				<div class="panel-footer">
					<a href="/user/settings" class="btn btn-success pull-right"><span class="glyphicon glyphicon-pencil"></span>&nbsp; Edit </a>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>
	
