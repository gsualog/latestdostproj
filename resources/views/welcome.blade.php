<!doctype html>
<html lang="{{ app()->getLocale() }}">
   <head>
<!--
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Maintenance Checklist</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Custom CSS -->
    <link href="css/landing-page.css" rel="stylesheet">

    <style type="text/css">
        body {
            position: fixed;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            background: url(http://www.gigamein.com/images/home/img/jobPostLanding.jpg) no-repeat center center fixed;
            background-size: cover;
           /* -webkit-filter: blur(5px);*/
            
        }
        .row{
            color: white;
            text-align: center;
            margin-top:230px;
        }
        .row h1{
            font-size: 90px;
        }
        .pager li{
            margin-left: 5px;
            border-color: green;
        }
        .pager{
            margin-top: 42px;
            
        }
        .background{
            background-color: black;
            background:rgba(0,0,0,0.5);
            padding: 10px;
            /*border-width: 50px;*/
            border-radius: 100px;
        }
        .content{
            font-size: 16px;
        }


    </style>
    
</head>

<body>
    <div class="container">
        <div class="row">
            <h1>Maintenance Checklist</h1>
            <h3>USE OF MAINTENANCE CHECKLIST</h3>
            <div class="background">
                <ul style="list-style-type:none">
                    <li><p class="content">The Maintenance Checklist is a tool used to check the condition of the office's structure, furniture equipment, and other facilities.</p></li>
                    <li><p class="content">It is not used only when there is something that needs repair.</p></li>
                    <li><p class="content">Every office should designate a person-in-charge of the<b> WEEKLY CHECK-UP </b></p></li>
                    <li><p class="content">The check-up should be done <b>EVERY MONDAY MORNING</b> and should be submitted before 12PM</p></li>
                </ul>
            </div>
        </div>
        <div>
            <ul class="pager">
                <li><a  href="/admin"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
                <li><a  href="/">1</a></li>
                <li><a  href="/login"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
            </ul>
        </div>
    </div>
    
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>

</body>


</html>

