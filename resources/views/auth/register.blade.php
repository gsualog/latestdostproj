<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Office Login</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
      
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>

    </head>
     <style type="text/css">
        @import url(https://fonts.googleapis.com/css?family=Roboto:400,300,100,700,500);
        @import url(https://fonts.googleapis.com/css?family=Open+Sans+Condensed:700);
       body {
            position: fixed;
            width: 100%;
            height: 100%;
            /*background: url(http://www.gigamein.com/images/home/img/jobPostLanding.jpg) no-repeat center center fixed;*/
            background-size: cover;
            /*-webkit-filter: blur(3px); */
            /*background-color: #b0c0c1;*/
            
        }
        .row{
            color:white;
            text-align: center;
            margin-top:100px;
        }
        .row h1{
            font-size: 90px;
        }


        #name{
          margin-top: 50px;
        }
        
        #submit{
          margin-bottom: 40px;
        }

        form {
        position: relative;
        /*width: 250px;*/
        margin: 0 auto;
        background-color: #3e4444;
        padding: 20px 22px;
        border: 1px solid;
        border-top-color: rgba(255,255,255,.4);
        border-left-color: rgba(255,255,255,.4);
        border-bottom-color: rgba(60,60,60,.4);
        border-right-color: rgba(60,60,60,.4);
      }

      form input, form button {
        width: 212px;
        border: 1px solid;
        border-bottom-color: rgba(255,255,255,.5);
        border-right-color: rgba(60,60,60,.35);
        border-top-color: rgba(60,60,60,.35);
        border-left-color: rgba(80,80,80,.45);
        background-color: rgba(0,0,0,.2);
        background-repeat: no-repeat;
        padding: 8px 24px 8px 10px;
        font: bold .875em/1.25em "Open Sans Condensed", sans-serif;
        letter-spacing: .075em;
        color: #fff;
        text-shadow: 0 1px 0 rgba(0,0,0,.1);
        margin-bottom: 19px;
      }

      form input:focus { background-color: rgba(0,0,0,.4); }


      ::-webkit-input-placeholder { color: #ccc; text-transform: uppercase; }
      ::-moz-placeholder { color: #ccc; text-transform: uppercase; }
      :-ms-input-placeholder { color: #ccc; text-transform: uppercase; }

      form button[type=submit] {
        width: 248px;
        margin-bottom: 0;
        /*color: #3f898a;*/
        letter-spacing: .05em;
        text-shadow: 0 1px 0 #133d3e;
        text-transform: uppercase;
        /*background: #225556;*/
        border-top-color: #9fb5b5;
        border-left-color: #608586;
        border-bottom-color: #1b4849;
        border-right-color: #1e4d4e;
        cursor: pointer;
      }
    </style>
<body>
  @include('layouts.adminApp')
  <div id="bg"></div>
  <div class="container">
   <div class="row">
    <div class="col-md-6 col-md-offset-3">
            <form id="register-form" class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                 {{ csrf_field() }}
                 
                <h2><b>Register Office</b></h2>
                  <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <input id="name" type="text" class="form-control" tabindex="1" name="name" value="{{ old('name') }}" placeholder="Office Name" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                    <!-- <input type="text" name="name" id="name" tabindex="1" class="form-control" placeholder="Office Name" value=""> -->
                  </div>
                  <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="password" tabindex="2" class="form-control" name="password" placeholder="Password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                    <!-- <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password"> -->
                  </div>
                  <div class="form-group">
                    <!-- id= type="password" class="form-control" name="password_confirmation" -->
                    <input type="password" name="password_confirmation" id="password-confirm" tabindex="2" class="form-control" placeholder="Confirm Password" required>
                  </div>
                  <div class="form-group" id="submit">  
                        <button type="submit" class="form-control btn btn-info">
                            Register
                        </button>       
                    </div>
              </form>
      
    </div>
  </div>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
</body>
</html>




