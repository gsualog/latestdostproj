<nav class="navbar navbar-inverse navbar-static-top">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

                    <!-- Branding Image -->
                <a class="navbar-brand" href="/itemAnswers">
                  Maintenance Admin
                </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                &nbsp;
            </ul>
             <!-- Right Side Of Navbar -->
             <ul class="nav navbar-nav navbar-right">
                 <!-- Authentication Links -->
                
                        <li class="dropdown" >
                         
                             <a class="btn btn-default"style="padding:5px;margin:10px ;" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                           <span class="glyphicon glyphicon-off" style="padding:5px;"></span>
                              </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                     </li>
                 <li style="padding-right:20px;"><a href="/itemAnswers" class="btn btn-primary" style="padding:5px;margin:10px 10px;">
                     <span class="glyphicon glyphicon-home" style="padding:5px; color:black;"></span>
                 </a></li>

                 
             </ul>
         </div>
    </nav>      