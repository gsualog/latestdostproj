<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>Maintenance Checklist</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    <style type="text/css">
      body {
          font-family: 'trebuchet MS', 'Lucida sans', Arial;
          font-size: 14px;

      }
      /*#bg{
         position: fixed;
         left: 0;
         top: 0;
         width: 100%;
         height: 100%;
         background: url(http://www.4300maintenancecompany.com/banner2.png) no-repeat center center fixed;
         background-size: cover;
         -webkit-filter: blur(5px);
      }*/
         

      table {
          /* IE7 and lower */
          border-spacing: 0;
          width: 100%; 
          margin-top: 50px;   
      }

    
    #itemlist td, #itemlist th {
        padding: 10px;
        border-bottom: 1px solid #f2f2f2;    
      }

    #itemlist tbody tr:nth-child(even) {
        background: #f5f5f5;
        -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
        -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset;  
        box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    }

    #itemlist th {
        text-align: left;
        text-shadow: 0 1px 0 rgba(255,255,255,.5); 
        border-bottom: 1px solid #ccc;
        background-color: #eee;
        background-image: -webkit-gradient(linear, left top, left bottom, from(#f5f5f5), to(#eee));
        background-image: -webkit-linear-gradient(top, #f5f5f5, #eee);
        background-image:    -moz-linear-gradient(top, #f5f5f5, #eee);
        background-image:     -ms-linear-gradient(top, #f5f5f5, #eee);
        background-image:      -o-linear-gradient(top, #f5f5f5, #eee); 
        background-image:         linear-gradient(top, #f5f5f5, #eee);
    }

    #itemlist th:first-child {
        -moz-border-radius: 6px 0 0 0;
        -webkit-border-radius: 6px 0 0 0;
        border-radius: 6px 0 0 0;  
    }

    #itemlist th:last-child {
        -moz-border-radius: 0 6px 0 0;
        -webkit-border-radius: 0 6px 0 0;
        border-radius: 0 6px 0 0;
    }

    #itemlist th:only-child{
        -moz-border-radius: 6px 6px 0 0;
        -webkit-border-radius: 6px 6px 0 0;
        border-radius: 6px 6px 0 0;
    }

    #itemlist tfoot td {
        border-bottom: 0;
        border-top: 1px solid #fff;
        background-color: #f1f1f1;  
    }

    #itemlist tfoot td:first-child {
        -moz-border-radius: 0 0 0 6px;
        -webkit-border-radius: 0 0 0 6px;
        border-radius: 0 0 0 6px;
    }

    #itemlist tfoot td:last-child {
        -moz-border-radius: 0 0 6px 0;
        -webkit-border-radius: 0 0 6px 0;
        border-radius: 0 0 6px 0;
    }

    #itemlist tfoot td:only-child{
        -moz-border-radius: 0 0 6px 6px;
        -webkit-border-radius: 0 0 6px 6px
        border-radius: 0 0 6px 6px
    }

    .wrapper {
    min-height: 2000px;
    }

    .left-sidebar {
    position: fixed;
    left: 0px;
    top:70px;
    width: 240px;
    height: 100%;
    overflow: hidden;
    -moz-box-shadow: 1px 4px 5px 4px rgba(0,
    0,
    0,
    0.3);
    -webkit-box-shadow: 1px 4px 5px 4px rgba(0,
    0,
    0,
    0.3);
    box-shadow: 1px 4px 5px 4px rgba(0,
    0,
    0,
    0.3);
    overflow-y: scroll;
    transition: 0.5s;
    z-index: 99;
    }
    .left-sidebar::-webkit-scrollbar {
        width: 6px;
        background-color: #ECECEC;
    }
    .left-sidebar::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    }
    .left-sidebar::-webkit-scrollbar-thumb {
        background-color: #3e4444;
        outline: 1px solid black;
    }
    .left-sidebar .side-nav {
        margin: 0px;
        padding: 0px;
    }
    .left-sidebar .side-nav li {
        width: 100%;
        list-style-type: none;
        padding: 0px;
        /*border-radius:5px;*/
    }
    .panel {
        margin-bottom: 0px;
        background-color: #ECECEC;
        border-top: 1px solid #777;
        border-radius: 0px;
        -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
        box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
    }

  
    #printableArea{
      margin-top: 150px;
    }

    #dropdown-toggle{
      padding-right: 10px;
    }

    #print{
      margin-left: 670px;
    }
    .btn-label {position: relative;
                left: -10px;
                display: inline-block;
                padding: 6px 12px;
                background: rgba(0,0,0,0.15);
                border-radius: 3px 0 0 3px;}
    .btn-labeled {padding-top: 0;padding-bottom: 0;}
    </style>

    <style type="text/css">
      .glyphicon-lg{font-size:5em}
      .blockquote-box{border-right:5px solid #E6E6E6;margin-bottom:25px;}
      .blockquote-box .square{width:120px;min-height:60px;margin-right:22px;text-align:center!important;background-color:#E6E6E6;padding:20px 0;}
     /* .blockquote-box.blockquote-primary{border-color:#357EBD}
      .blockquote-box.blockquote-primary .square{background-color:#428BCA;color:#FFF}
      .blockquote-box.blockquote-success{border-color:#4CAE4C}
      .blockquote-box.blockquote-success .square{background-color:#5CB85C;color:#FFF}
      .blockquote-box.blockquote-info{border-color:#46B8DA}
      .blockquote-box.blockquote-info .square{background-color:#5BC0DE;color:#FFF}
      .blockquote-box.blockquote-warning{border-color:#EEA236}
      .blockquote-box.blockquote-warning .square{background-color:#F0AD4E;color:#FFF}
      .blockquote-box.blockquote-danger{border-color:#D43F3A}
      .blockquote-box.blockquote-danger .square{background-color:#D9534F;color:#FFF}*/
    </style>

    <style type="text/css">
      @import url(https://fonts.googleapis.com/css?family=Nunito:300);

      #button{
        text-transform: uppercase;
        letter-spacing: 2px;
        text-align: center;
        color: #0C5;

        font-size: 24px;
        font-family: "Nunito", sans-serif;
        font-weight: 300;
        
        margin: 100px 640px;
        
        position: absolute; 
        top:0; right:0; bottom:0; left:0;
        
        padding: 10px 10px;
        width: 290px;
        height:70px;

        background: #0D6;
        border: 1px solid #0D6;
        color: #FFF;
        overflow: hidden;
        
        transition: all 0.5s;
      }

      #button:hover, #button:active{
        text-decoration: none;
        color: #0C5;
        border-color: #0C5;
        background: #FFF;
      }

      #button p {
        display: inline-block;
        position: relative;
        padding-right: 0;
        
        transition: padding-right 0.5s;
      }

      #button p:after{
        content: ' ';  
        position: absolute;
        top: 0;
        right: -18px;
        opacity: 0;
        width: 10px;
        height: 10px;
        margin-top: -10px;

        background: rgba(0, 0, 0, 0);
        border: 3px solid #FFF;
        border-top: none;
        border-right: none;

        transition: opacity 0.5s, top 0.5s, right 0.5s;
        transform: rotate(-45deg);
      }

      #button:hover p, #button:active p{
        padding-right: 30px;
      }

      #button:hover p:after, #button:active p:after{
        transition: opacity 0.5s, top 0.5s, right 0.5s;
        opacity: 1;
        border-color: #0C5;
        right: 0;
        top: 50%;
      }

      #office{
        text-transform: uppercase;
        letter-spacing: 2px;
        text-align: center;
        color: #36486b;

        font-size: 24px;
        font-family: "Nunito", sans-serif;
        font-weight: 300;
        
        margin: 100px 936px;
        
        position: absolute; 
        top:0; right:0; bottom:0; left:0;
        
        padding: 10px 10px;
        width: 290px;
        height:70px;

        background: #034f84;
        border: 1px solid #034f84;
        color: #FFF;
        overflow: hidden;
        
        transition: all 0.5s;
      }

      #office:hover, #office:active{
        text-decoration: none;
        color: #36486b;
        border-color: #36486b;
        background: #FFF;
      }

      #office p {
        display: inline-block;
        position: relative;
        padding-right: 0;
        
        transition: padding-right 0.5s;
      }

      #office p:after{
        content: ' ';  
        position: absolute;
        top: 0;
        right: -18px;
        opacity: 0;
        width: 10px;
        height: 10px;
        margin-top: -10px;

        background: rgba(0, 0, 0, 0);
        border: 3px solid #FFF;
        border-top: none;
        border-right: none;

        transition: opacity 0.5s, top 0.5s, right 0.5s;
        transform: rotate(-45deg);
      }

      #office:hover p, #office:active p{
        padding-right: 30px;
      }

      #office:hover p:after, #office:active p:after{
        transition: opacity 0.5s, top 0.5s, right 0.5s;
        opacity: 1;
        border-color: #36486b;
        right: 0;
        top: 50%;
      }
    </style>

    <style type="text/css">
      #register{
        text-transform: uppercase;
        letter-spacing: 2px;
        text-align: center;
        color: #c63939;

        font-size: 24px;
        font-family: "Nunito", sans-serif;
        font-weight: 300;
        
        /*margin: 5em  auto;*/
        margin: 100px 374px;
        position: absolute; 
        top:0; right:0; bottom:0; left:0;
        
        padding: 15px 15px;
        width: 260px;
        height:70px;

        background: #cc3333;
        border: 1px solid #cc3333;
        color: #FFF;
        overflow: hidden;
        
        transition: all 0.5s;
      }

      #register:hover, #register:active {
        text-decoration: none;
        color: #c63939;
        border-color: #c63939;
        background: #FFF;
      }

      #register p{
        display: inline-block;
        position: relative;
        padding-right: 0;
        
        transition: padding-right 0.5s;
      }

      #register p:after {
        content: ' ';  
        position: absolute;
        top: 0;
        right: -18px;
        opacity: 0;
        width: 10px;
        height: 10px;
        margin-top: -10px;

        background: rgba(0, 0, 0, 0);
        border: 3px solid #FFF;
        border-top: none;
        border-right: none;

        transition: opacity 0.5s, top 0.5s, right 0.5s;
        transform: rotate(-45deg);
      }

      #register:hover p, #register:active p {
        padding-right: 30px;
      }

      #register:hover p:after, #register:active p:after{
        transition: opacity 0.5s, top 0.5s, right 0.5s;
        opacity: 1;
        border-color: #c63939;
        right: 0;
        top: 50%;
      }
    </style>

</head>

<body>
	 @include('layouts.adminhomeApp') 

  <!-- <div id="bg"></div> -->
   <div class="wrapper" id="wrapper">
    <div class="left-container" id="left-container">
      
      <div class="left-sidebar" id="show-nav">
        <ul id="side" class="side-nav">
          <li class="panel panel-primary">
            <h4 class="panel-heading"><i class="material-icons">computer</i>&nbsp;&nbsp;&nbsp;Offices Submitted</h4>
          </li>
        </br>
           @forelse($showOffices as $showOffice)
          <li class="panel panel-success">
            <h4 class="panel-heading">
              <i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;&nbsp;{{ $showOffice->name }}
            </h4>
          </li>
    
          @empty
            <h4 style="padding-left:12px;">No Results Found</h4>
          @endforelse
          
        </ul>
      </div>
     
    </div>

  <div class="container">
   
      <div class="row">
         <!-- <div class="col-md-6"> -->
        <a href="/register"  id="register">
          <p><i class="glyphicon glyphicon-user"></i>&nbsp;Register</p>
        </a>
        <a href="/items/archiveFiles" id="button" >
          <p><i class="glyphicon glyphicon-folder-open"></i>&nbsp;Archive Files</p>
        </a>
        <a href="/user/profiles"  id="office" >
          <p><i class="glyphicon glyphicon-briefcase"></i>&nbsp;Offices</p>
        </a>
      <!-- </div> -->
    </div>
      

      <div id="printableArea">
        <div class="row">
           <div class="col-md-7 col-md-offset-3">
            <div class="blockquote-box clearfix">
                <div class="square pull-left">
                    <span class="glyphicon glyphicon-wrench glyphicon-lg"></span>
                </div>
                <h1 style="text-align:center;"> List of Equipments for Repair</h1>
                   
              
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 col-md-offset-1" >
              <table class="table table-responsive table-bordered" id="itemlist" >
                <thead>
                  <tr>
                      <th>Date</th>
                      <th>Items</th>
                      <th>Details</th>
                      <th>Location</th>
                  </tr>
                </thead>
                <tbody>
                    
                    @forelse($itemAnswers as $itemAnswer)
                    <tr>
                      <td>{{ $itemAnswer->date }}</td>
                      <td>{{ $itemAnswer->code }}</td>
                      <td>{{ $itemAnswer->details }}</td>
                      <td>{{ $itemAnswer->name }}</td>

                    @empty
                      No Results Found
                      </tr>
                    @endforelse

                </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-xs-6 col-sm-6 col-md-6" id="print">
        <button type="button" class="btn btn-md btn-labeled btn-success pull-right"  onclick="printDiv('printableArea')">
          <span class="btn-label"><i class="glyphicon glyphicon-print"></i></span>Print</button>
      <!-- <button class="btn btn-success "  >Print </button> -->
    </div>
  </div>
</body>

  <script type="text/javascript">
  function printDiv(divName){
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;

    document.body.innerHTML = printContents;
    window.print();

    document.body.innerHTML = originalContents;
  }
  </script>
</html>