<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <title>Admin</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
        <style type="text/css">
            .search{
                text-align: center;
            }

            .admin{
                padding-left: 50px;
            }
            .toolbarRight{
                padding-right: 20px;
            }

            .topText{
                padding-top: 70px;

            }
            /*body{
                position: fixed;
                width: 100%;
                height: 100%;
               
                background-size: cover;
                
                background-color: #a8dbdf;
            }*/
        </style>
    </head>
    <body>
        <nav class="navbar navbar-inverse navbar-static-top">   
                <div class="navbar-header">
                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                      Maintenance Checklist
                    </a>
                </div>

                <div class="collapse navbar-collapse " id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <!-- <li><a href="/admin/login">Login</a></li> -->
                            <!-- <li><a href="{{ route('register') }}">Register Office</a></li> -->
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                   Admin <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                    <li><a href="/register">Add Office</a></li>
                                    <li><a href="/items/archiveFiles">Archive Files </a></li>
                                    <!-- <li><a href="/user/viewAllProfile">Edit Office Profile</a></li> -->
                                   <!--  <li><a href="/profile/{{ Auth::user()->name }}">My profile</a></li> -->
                                    <!-- <li><a href="/admin/settings">Profile Settings</a></li> -->
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container">
            <div class="row topText">
                <div class="col-lg-12">
                    <h3 class="search">Search Form Database</h3>
                </div>
            </div>
            <br/>
            <br/>
            <div class="row">
                <div class="col-lg-4 col-lg-offset-4">
                    <form>
                      <div class="input-group">
                        <input id="search" type="search" class="form-control" name="search" placeholder="Enter to Search" value="">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                      </div>
                    </form>
                    <!-- <input type="search" id="search" value="" class="form-control" placeholder="Enter to Search"><span class="glyphicon glyphicon-search" ></span> -->
                </div>
            </div>
            <br/>
            <br/>
            <div class="row">
                <div class="col-lg-12">
                    <table class="table" id="table">
                        <thead>
                            <tr>
                                <th>Year</th>
                                <th>Month</th>
                                <th>Week</th>
                                <th>Form</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>2015</td>
                                <td>March</td>
                                <td>1st</td>
                                <td> <a href="#">View</a></td>
                            </tr>
                            <tr>
                                <td>2017</td>
                                <td>January</td>
                                <td>4th</td>
                                <td> <a href="#">View</a></td>
                            </tr>
                            <tr>
                                <td>2018</td>
                                <td>December</td>
                                <td>3rd</td>
                                <td> <a href="#">View</a></td>
                            </tr>
                            <tr>
                                <td>2017</td>
                                <td>December</td>
                                <td>2nd</td>
                                <td> <a href="#">View</a></td>
                            </tr>
                        </tbody>
                       <!--  $users = DB::table('users')->get();

                        foreach ($users as $user)
                        {
                            var_dump($user->name);
                        } -->
                    </table>
                    <hr>
                </div>
            </div>
         </div>
    <script src="//rawgithub.com/stidges/jquery-searchable/master/dist/jquery.searchable-1.0.0.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $( '#table' ).searchable({
                striped: true,
                oddRow: { 'background-color': '#f5f5f5' },
                evenRow: { 'background-color': '#fff' },
                searchType: 'fuzzy'
            });
            
            $( '#searchable-container' ).searchable({
                searchField: '#container-search',
                selector: '.row',
                childSelector: '.col-xs-4',
                show: function( elem ) {
                    elem.slideDown(100);
                },
                hide: function( elem ) {
                    elem.slideUp( 100 );
                }
            })
        });
    </script>
    </body>
</html>
