<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/user/profiles', 'ProfileController@viewAllProfile');

// Route::get('/profile/{name}', 'ProfileController@profile');

Route::get('/user/settings/{id}', 'ProfileController@settings');

// Route::put('/user/{id}', 'ProfileController@update');

Route::resource('/profiles', 'ProfileController');
// Route::get('/home', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');


Route::get('/items/delete/{id}', 'ItemController@toDelete');

Route::get('/items/archive', 'ItemController@archive');

Route::get('/items/archiveFiles', 'ItemController@archiveOffice');


// admin
Route::get('admin/home', 'AdminController@index');

Route::get('admin', 'Admin\LoginController@showLoginForm')->name('admin.login');

Route::post('admin', 'Admin\LoginController@login');

Route::post('admin-password/email', 'Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');

Route::get('admin-password/reset', 'Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');

Route::post('admin-password/reset', 'Admin\ResetPasswordController@reset');

Route::get('admin-password/reset/{token}', 'Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');

// Route::get('/itemsAnswers', 'ItemController@showOffice');

Route::resource('/itemAnswers', 'ItemAnswerController');

Route::resource('/items', 'ItemController');

Route::resource('/categories', 'CategoryController');


