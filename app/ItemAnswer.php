<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemAnswer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;
    protected $fillable = [
        'item_id', 'form_id', 'condition', 'details',
    ];
}
