<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use Auth;
use Admin;

class ProfileController extends Controller
{
    // public function profile($name){
    //  $user = User::whereName($name)->first();
    //  return view('users.profile', compact('user'));
    // }
    /**
    * @param int $id
    * @return \Illuminate\Http\Response
    **/
    public function settings($id){
        $user = User::find($id);
        return view('users.edit', compact('user'));
    }

    public function update(Request $request, $id){
        // $user = User::find($id);
        // $user['name'] = $request->name;
        // $user['password'] = bcrypt($request->password);

        $item = DB::table('users')->where('id', '=', $id)->update(array('name' => $user = $request->input('name'), 'password' =>$user = bcrypt($request->input('password'))));
       

        // $request->merge(array('password' => bcrypt($request->password)));
        // $user->update($request->all());
        return redirect('/user/profiles');
    }
     /**
    * 
    * @return \Illuminate\Http\Response
    **/
    public function viewAllProfile(){
        // $check = DB::table('users')->where('name', '=', $user = Auth::user()->name)->get();
        // if($check == false){

            $users = DB::table('users')->orderBy('id', 'asc')->get();
            return view('users.viewAllProfile', compact('users'));
        // }
        // else{
        //     //echo ("Only ADMIN can access  this!");
        //     return redirect('/home');
        // }


    }
}