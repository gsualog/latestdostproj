<?php

namespace App\Http\Controllers;

use App\ItemAnswer;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Auth;

class ItemAnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $showOffices = DB::table('item_answers')->select('users.name')->join('items', 'items.id','=','item_answers.item_id')->join('forms', 'forms.id','=','item_answers.form_id')->join('users', 'users.id','=','items.office_id')->where('forms.date' ,'=', date("Y-m-d"))->distinct()->get();
        $itemAnswers = DB::table('item_answers')->select('item_answers.id', 'items.code', 'forms.date', 'item_answers.details', 'users.name')->join('items', 'items.id','=','item_answers.item_id')->join('forms', 'forms.id','=','item_answers.form_id')->join('users', 'users.id','=','items.office_id')->orderBy('forms.date' , 'desc')->get();
        return view('itemAnswers.index')->with(compact('showOffices', 'itemAnswers'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        ItemAnswer::create($request->all());

        return redirect('/itemAnwers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
